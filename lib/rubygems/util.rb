# This file is only here as a workaround for this bug:
# https://github.com/janlelis/unicode-display_width/issues/18
require 'zlib'
require 'stringio'

module Gem
  module Util
    def self.gunzip arg
      Zlib::GzipReader.new(StringIO.new(arg)).read
    end
  end
end
