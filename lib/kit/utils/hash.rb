module KIT
  module Utils
    module Hash

      def map_values hash
        map_pairs(hash) {|key, value| [key, yield(key, value)]}
      end

      def map_keys hash
        map_pairs(hash) {|key, value| [yield(key, value), value]}
      end

      def map_pairs(hash, &block)
        out = hash.class.new
        hash.map(&block).each {|key, value| out[key] = value}
        out
      end

      def symbolize_keys hash
        map_keys(hash) {|key, _| key.to_sym}
      end

      def deep_symbolize_keys obj
        if obj.is_a?(::Hash)
          hash = symbolize_keys obj
          map_values(hash) {|_, value| deep_symbolize_keys(value)}
        elsif obj.is_a?(::Array)
          obj.map {|obj2| deep_symbolize_keys obj2}
        else
          obj
        end
      end
    end
  end
end
