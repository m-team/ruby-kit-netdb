require 'optparse'
require 'ostruct'
require 'open3'
require 'openssl'
require 'terminal-table'
require 'io/console'
require 'csv'

require 'kit/netdb/api'
require 'kit/netdb/version'


module KIT::NetDB
  class CLI

    def initialize(argv)
      @options = OpenStruct.new

      # Default option values
      options.action = :call
      options.version = "2.1"
      options.output = :table
      options.url = "https://www-net.scc.kit.edu"
      options.tls = true
      options.no_wrap = false
      options.show_empty = false
      options.no_legend = false
      options.width = 1/0.0 # Infinity

      if STDOUT.isatty
        options.width = IO.console.winsize[1]
      end

      OptionParser.new do |cli|
        cli.version = KIT::NetDB::VERSION
        cli.banner = "Usage: #{cli.program_name} [OPTIONS] SYSTEM OBJECT_TYPE FUNCTION [ARG=VALUE..] \n" \
                     "    Execute the function SYSTEM/OBJECT_TYPE/FUNCTION on the NetDB server"


        cli.separator "\nOutput Control:"

        cli.on "-o", "--output=TYPE", String,
               "Output format type. Must be one of 'table', 'json', 'csv' and 'ruby'.",
               "Defaults to '#{options.output}'. " do |t|
          raise OptionParser::InvalidArgument.new t unless ["table", "json", "csv", "ruby"].include? t
          options.output = t.to_sym
        end

        cli.on "-f", "--fields=[FIELD..]", Array,
               "Comma-seperated list of fields to include in output. By default",
               "all fields are printed. Implies `--show-empty'." do |fields|
          options.fields = fields.map {|f| f.to_sym}
          options.show_empty = true
        end

        cli.on "-w", "--table-width=COLUMNS", Integer,
               "Maximal width of the table. If stdout is a terminal, defaults to",
               "the terminal width, otherwise it is unlimited." do |columns|
          options.width = columns
        end

        cli.on "--no-wrap",
               "Disable wrapping of long lines in table output." do
          options.no_wrap = true
        end

        cli.on "--show-empty",
               "Also include fields that are empty in all results in the output.",
               "By default, they are skipped." do
          options.show_empty = true
        end

        cli.on "--no-legend",
               "By default, there is a legend shown with table output if there",
               "are abbreviations used in the table. This options disables the",
               "legend." do
          options.no_legend = true
        end




        cli.separator "\nNetDB Options:"

        cli.on "-l", "--url=URI", String,
               "URL where to connect to the WebAPI. Defaults to '#{options.url}'." do |url|
          options.url = url
          options.tls = url.start_with? 'https://'
        end

        cli.on "--api-version=VERSION", String,
               "WebAPI version to use. Only '2.{0,1}' is guaranteed to work. Use at",
               "your own risk. Defaults to '#{options.version}'." do |v|
          options.version = v
        end

        cli.on "-p", "--params",
               "Show available parameters for the NetDB function." do
          options.action = :params
        end

        cli.on "-i", "--index",
               "Show the the available systems/objects/functions for the given system/object." do
          options.action = :index
        end

        cli.separator "\nAuthentication Options:"

        # TODO report Optparse bug: Option spec (without description) can't be too long or --help will crash
        cli.on "-c", "--client-cert=CERT[,KEY]", Array,
               "Use a TLS client certificate to authenticate with the NetDB. Read",
               "the DER or PEM encoded X509 certificate and key from the files at",
               "CERT and KEY, respectively. If KEY is ommited, then the file at",
               "CERT must contain both certificate and secret key." do |paths|

          raise OptionParser::InvalidArgument, "Too many values" if paths.length > 2

          options.cert = OpenSSL::X509::Certificate.new File.read paths[0]
          options.key = OpenSSL::PKey.read File.read (paths[1] or paths[0])
        end

        cli.on "--client-cert-p12=PAIR", String,
               "Use a TLS client certificate to authenticate with the NetDB. Read",
               "the PKCS#12 encoded X509 certificate/key pair from the file at PAIR." do |path|

          pkcs12 = OpenSSL::PKCS12.new File.read(path), ""
          options.cert = pkcs12.certificate
          options.key = pkcs12.key
        end

        cli.on "-g", "--client-cert-gpg=PATTERN", String,
               "Use a TLS client certificate to authenticate with the NetDB.",
               "Retrieve the X509 certificate/key pair from GnuPG via gpgsm(1),",
               "providing PATTERN as the search term for the GnuPG keyring. The",
               "PATTERN should only match a single certificate/key pair in the",
               "keyring. Think `gpgsm --export[-secret-key-raw] PATTERN`." do |pattern|

          raw_cert, stderr, status = Open3.capture3("gpgsm", "--export", pattern)
          raise RuntimeError, "Failed to export client certificate from GnuPG\n#{stderr}" \
            unless status.exited? and status.exitstatus == 0

          raw_key, stderr, status = Open3.capture3("gpgsm", "--export-secret-key-raw", pattern)
          raise RuntimeError, "Failed to export client certificate key from GnuPG\n#{stderr}" \
            unless status.exited? and status.exitstatus == 0

          options.cert = OpenSSL::X509::Certificate.new raw_cert
          options.key = OpenSSL::PKey.read raw_key
        end


        cli.separator "\nCommon Options:"

        cli.on_tail "-?", "--help",
                    "Print this help text" do
          puts cli
          exit
        end

        cli.on_tail "-V", "--version",
                    "Print the program version" do
          puts "#{cli.program_name} #{cli.version}"
          puts "WebAPI #{options.version}"
          exit
        end

      end.parse! argv

      # Main Arguments
      @system = argv.shift or options.action == :index or raise OptionParser::MissingArgument, "SYSTEM"
      @object_type = argv.shift or options.action == :index or raise OptionParser::MissingArgument, "OBJECT_TYPE"
      if @function = argv.shift then
        raise OptionParser::InvalidOption, "--index can't be used with functions" if options.action == :index
      else
        raise OptionParser::MissingArgument, "FUNCTION" unless options.action == :index
      end

      # Function Parameters
      @args = {}
      argv.each do |arg|
        tuple = arg.split('=')
        raise OptionParser::InvalidArgument, arg unless tuple.length == 2
        args[tuple[0].to_sym] = tuple[1]
      end

    end

    def run
      KIT::NetDB::API.connect options.url, use_ssl: options.tls,
                              cert: options.cert, key: options.key do |connection|
        entity = connection.api
                   .version(options.version)

        entity = entity.system(system) unless system.nil?
        entity = entity.object_type(object_type) unless object_type.nil?
        entity = entity.index[function.to_sym] unless function.nil?

        case options.action
        when :call then
          rsp = entity.call(**args)

          unless options.show_empty
            rsp = rsp.map {|elt| elt.reject {|_,v| v.nil? || v.respond_to?(:empty?) && v.empty?}}
          end

          if options.fields
            rsp = rsp.map {|elt| elt.select {|k,_| options.fields.include?(k)}}
          end

          case options.output
          when :ruby then pp rsp
          when :json then puts rsp.to_json
          when :table then
            if rsp.is_a? Array and rsp.all? {|elt| elt.is_a? Hash}
              table = Terminal::Table.new do |t|
                headings = rsp.map {|elt| elt.keys}.flatten.uniq.sort
                field_width = options.width / headings.length

                t.headings = headings.map {|k| word_wrap(k.to_s.gsub("_", " "), field_width)}
                t.rows = rsp.map {|elt| headings.map {|k| word_wrap(in_row_fmt(k, elt[k]), field_width)}}
              end unless rsp.empty?
            else
              puts rsp.to_json
              raise RuntimeError, "Cannot format response as table. See `--output' for alternatives. Using `--output=json' for now."
            end

            puts table

            unless legend.empty? or options.no_legend
              puts "LEGEND:"
              legend.sort.each do |category, abbrevs|
                puts " #{category}:"
                abbrevs.sort.each do |value, description|
                  puts "  #{value}: #{description}"
                end
              end
            end

          when :csv then
            if rsp.is_a? Array
              CSV do |csv|
                rsp.each do |elt|
                  if elt.is_a? Hash
                    csv << elt.map {|k, v| "#{k}=#{in_row_fmt v}"}
                  elsif elt.is_a? Array
                    csv << elt.map {|e| e.to_s}
                  else
                    puts rsp.to_json
                    raise RuntimeError, "Cannot format response as CSV. See `--output' for alternatives. Using `--output=json' for now."
                  end
                end
              end
            else
              puts rsp.to_json
              raise RuntimeError, "Cannot format response as CSV. See `--output' for alternatives. Using `--output=json' for now."
            end
          end

        when :params then
          puts "Available parameters for #{entity.path.split("/").drop(2).join(" > ")}:"
          if entity.is_a? API::Function::Writer
            table = Terminal::Table.new do |t|
              t.style = {border_y: "  ", border_x: " ", border_i: " ", border_top: false, border_bottom: false}
              t.headings = ['COMMAND LINE', 'TYPE', 'DEFAULT', 'REQUIRED?', 'DESCRIPTION']
              t.rows = entity.params
                         .map {|id, spec| [
                                 "#{id}=#{spec[:datatype][:name].upcase}",
                                 word_wrap(spec[:datatype][:description], 20),
                                 spec[:default],
                                 spec[:is_required],
                                 word_wrap(spec[:description], 80),
                               ]}
            end
          else
            table = Terminal::Table.new do |t|
              t.style = {border_y: "  ", border_x: " ", border_i: " ", border_top: false, border_bottom: false}
              t.headings = ['COMMAND LINE', 'DEFAULT','DESCRIPTION']
              t.rows = entity.params
                         .map {|id, spec| [
                                 "#{id}=TEXT",
                                 spec[:default],
                                 word_wrap(spec[:description], 80),
                               ]}
            end
          end

          puts table

        when :index then
          table = Terminal::Table.new do |t|
            type = if system.nil?
                     "systems"
                   elsif object_type.nil?
                     "object types"
                   else
                     "functions"
                   end
            puts "Available #{type} for v#{entity.path.split("/").drop(1).join(" > ")}:"
            t.style = {border_y: "  ", border_x: " ", border_i: " ", border_top: false, border_bottom: false}
            t.rows = entity.index
                       .map {|id, sub_entity| [
                               id.to_s,
                               if sub_entity.respond_to? :name then sub_entity.name else (sub_entity.doc or "").strip end
                             ]}
          end

          puts table
        end

      end
    end

    private
    attr_accessor :options, :args, :system, :object_type, :function

    def word_wrap str, n
      # See: https://www.reddit.com/r/ruby/comments/6peuhh/how_to_split_string_after_n_characters_but_dont/
      if options.no_wrap || n == 1/0.0
        str
      else
        str.scan(/.{1,#{n}}.*?(?:\b|$)/).map {|s| s.strip}.join("\n")
      end
    end

    def in_row_fmt key=nil, obj
      if obj.is_a? Array
        obj.join ","
      elsif obj.is_a? Hash
        if key and obj.keys.sort == [:description, :value]
          (legend[key] ||= {})[obj[:value]] = obj[:description]
          obj[:value].to_s
        else
          obj.map {|k,v| "#{k}=#{v}"}.join " "
        end
      else
        obj.to_s
      end
    end

    def legend
      @legend ||= {}
    end
  end
end
