require 'net/http'
require 'json'

require 'kit/utils/hash'
require 'kit/netdb/api/root'

module KIT::NetDB::API

  def self.connect base_url, **args, &block
    url = URI base_url
    Net::HTTP.start(url.host, url.port, **args) do |http|
      connection = Connection.new(http, url)
      if block.arity == 0
        connection.instance_eval(&block)
      else
        yield connection
      end
    end
  end

  class Connection
    include KIT::Utils::Hash

    def initialize http, url
      @http = http
      @url = url
    end

    def api dir = "api"
      Root.new self, dir
    end

    def get path, **params
      uri = URI.join @url, path
      uri.query = URI.encode_www_form params

      parse @http.request Net::HTTP::Get.new(uri)
    end

    def post path, body
      uri = URI.join @url, path

      req = Net::HTTP::Post.new uri
      req.body = body.to_json
      req["Content-Type"] = "application/json"

      parse @http.request req
    end

    private

    def parse response
      response.value  # Raise error if not OK 2XX

      body = JSON.parse(response.body)
      if body.is_a? Hash and body.has_key? "error"
        r = deep_symbolize_keys JSON.parse(response.body)
        raise KIT::NetDB::FunctionalError.new(r), "Error executing function on server"
      else
        body
      end

    rescue Net::HTTPServerException, Net::HTTPFatalError => e
      if response.body
        begin
          r = deep_symbolize_keys JSON.parse(response.body)
          raise KIT::NetDB::FunctionalError.new(r), "Error executing function on server"

        rescue JSON::ParserError
         raise e, e.message + ": " + response.body
        end

      else
        raise
      end

    rescue JSON::ParserError
      response.body
    end
  end
end
