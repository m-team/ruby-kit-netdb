require 'kit/utils/hash'

module KIT::NetDB
  module API
    class Layer
      def initialize upper, *path
        @up = upper
        @connection = upper.connection
        @path = path.join '/'
        @id = path.last.to_sym
      end

      def do &block
        if block.arity > 0
          yield self
        else
          instance_eval(&block)
        end
      end

      attr_reader :path
      protected
      attr_reader :connection, :up, :id

      include KIT::Utils::Hash

      def get path = nil, **params
        if path
          connection.get "#{@path}/#{path}", **params
        else
          connection.get @path, **params
        end
      end

      def get_index **params
        get '', **params
      rescue Net::HTTPServerException => e
        if e.message.match? "^404"
          raise KIT::NetDB::IndexNotFoundError, "No index for #{@path}"
        else
          raise e
        end
      end

      def post path = '', body
        connection.post "#{@path}/#{path}", body
      end

      def self.sublayer type=nil, legacy_types={}, **types
        const = if const_defined?(:SUBLAYER_TYPES)
                  const_get(:SUBLAYER_TYPES).clone
                else
                  {}
                end

        const.default = type if type
        const.merge! types
        const.merge! legacy_types
        const_set :SUBLAYER_TYPES, const

        types.each do |id, t|
          define_sublayer_method(id)
        end
      end

      def self.define_sublayer_method(id)
        define_method id do
          sublayer id.to_s, nil
        end
      end

      def sublayer id, *args, type: self.class.const_get(:SUBLAYER_TYPES)[id.to_sym]
        type.new self, @path, id, *args
      end
    end
  end
end
