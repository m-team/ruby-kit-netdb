require 'kit/netdb/api/object_type_layer.rb'

module KIT::NetDB::API
  module ObjectTypes
    class Listable < ObjectTypeLayer
      sublayer list: Function::Inspectable,
               list_datastructure: Function::Inspector
    end

    class Mutable < Listable
      sublayer create: Function::Writer,
               delete: Function::Writer,
               update: Function::Writer
    end

    class BulkMutable < Listable
      sublayer bulk_create: Function::Writer,
               bulk_delete: Function::Writer,
               bulk_update: Function::Writer
    end

    class MutableAndSemiBulkMutable < Mutable
      sublayer bulk_delete: Function::Writer,
               bulk_update: Function::Writer
    end


    module WebAPI
      class DataDict < Listable
      end

      class Transaction < ObjectTypeLayer
        # TODO
      end

      class VersionStatus < Listable
      end
    end

    module DNSVS
      class FQDN < Mutable
      end

      class RecordIntType < Listable
      end

      class NameServer < Mutable
      end

      class ZoneDefinition < Mutable
      end

      class Manager2Range < Listable
        sublayer copy: Function::Writer,
                 bulk_delete: Function::Writer,
                 bulk_update: Function::Writer
      end

      class FQDNIntType < Listable
      end

      class ZoneDelegation < Listable
      end

      class IPAddressRange < Mutable
      end

      class Domain2Range < Listable
        sublayer bulk_update: Function::Writer
      end

      class IPAddress < BulkMutable
      end

      class ResourceRecord < Mutable
      end
    end

    module DHCPVS
      class SubnetOption < MutableAndSemiBulkMutable
      end

      class OptionGroup < MutableAndSemiBulkMutable
      end

      class MACFilter < MutableAndSemiBulkMutable
      end

      class OptionGroup2Lease < Mutable
      end

      class IPAddress < Listable
        sublayer bulk_update: Function::Writer
      end

      class Lease < MutableAndSemiBulkMutable
      end

      class GlobalOption < Mutable
      end

      class GroupOption < Mutable
      end
    end

  end
end
