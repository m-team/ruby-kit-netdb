require 'kit/netdb/api/version_layer'
require 'kit/netdb/api/systems'

module KIT::NetDB::API
  module Versions
    class Two < VersionLayer
      sublayer wapi: Systems::WebAPI,
               dns: Systems::DNSVS,
               dhcp: Systems::DHCPVS
               # TODO  nm: Systems::NetworkManagement,
               # TODO  nd: Systems::NetworkDokumentation
    end

    Default = Two
  end
end
