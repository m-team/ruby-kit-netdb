require 'kit/netdb/error'
require 'kit/netdb/api/layer'

module KIT::NetDB::API
  module Function
    def self.new *args, mode
      type = if mode.include? "read"
               Reader
             elsif mode.include? "write"
               Writer
             elsif mode.include? "inspect"
               Inspector
             else
               raise ArgumentError, "Unknown function mode #{mode}"
             end

      if mode.include? "inspectable"
        if type == Reader
          type = Inspectable
        else
          raise ServerError, "Inspectable #{type}s not supported, " \
                             "but server returned mode #{mode} for #{args[0..-3]}"
        end
      end

      type.new(*args)
    end

    class Base < Layer
      def initialize *args, doc
        super(*args)

        @doc = doc
      end

      # TODO Lazy fetching like for params
      attr_reader :doc

      def root
        up.up.up.up
      end

      def this_version
        up.up.up
      end

      def system
        up.up
      end

      def object_type
        up
      end
    end

    class Reader < Base
      def initialize *args, params
        super(*args)
        @params = deep_symbolize_keys params
      end

      def params
        @params ||= up.index[id].params
      end

      def call **params
        deep_symbolize_keys get(**params)
      rescue Net::HTTPServerException => e
        if e.message.match? "^404"
          raise KIT::NetDB::FunctionNotFoundError, "No function named #{id}"
        else
          raise e
        end
      end
    end

    class Inspectable < Reader
      def result
        @result ||= (
          object_type.inspector(:list_datastructure).call(function_name: id))
      end
    end

    class Writer < Base
      def initialize *args, params
        super(*args)
        @params = params if params and not params.empty?()
      end

      def params
        cooked_params = {}

        raw_params.each do |key, attrs|
          attrs2 = {
            :description => attrs[:description],
            :datatype => attrs[:datatype]}

          if attrs[:submitting][:old_value][:is_defined]
            cooked_params["old_#{key}".to_sym] = attrs2.merge(
              :default => attrs[:submitting][:old_value][:data_default],
              :is_required => attrs[:submitting][:old_value][:is_required])
          end

          if attrs[:submitting][:new_value][:is_defined]
            cooked_params["new_#{key}".to_sym] = attrs2.merge(
              :default => attrs[:submitting][:new_value][:data_default],
              :is_required => attrs[:submitting][:new_value][:is_required])
          end
        end

        cooked_params
      end

      def raw_params
        @params ||= (
          fn_spec = this_version.system(:wapi)
                      .object_type(:datadict)
                      .reader(:list)
                      .call(sys_name: system.id, ot_name: object_type.id, function_name: id)

          deep_symbolize_keys(
            fn_spec[system.id][:ObjectTypeDict][object_type.id][:FunctionDict][id][:ParameterDict]))
      end

      def call *args, **kwargs
        args += [kwargs]

        param_list = args.map do |arg|
          raw_params = {}

          arg.each do |key, value|
            if key.to_s.start_with?("new_")
              (raw_params[key.to_s.sub(/^new_/, "").to_sym] ||= {})[:new_value] = value
            elsif key.to_s.start_with?("old_")
              (raw_params[key.to_s.sub(/^old_/, "").to_sym] ||= {})[:old_value] = value
            else
              raise ArgumentError, "Invalid argument #{key}"
            end
          end

          raw_params.map {|k, v| v.merge name: k}
        end

        call_raw(*param_list)
      end

      def call_raw *params
        post(params.map {|param| {:param_list => param} })
      rescue Net::HTTPServerException => e
        if e.message.match? "^404"
          raise KIT::NetDB::FunctionNotFoundError, "No function named #{id}"
        else
          raise e
        end
      end
    end

    class Inspector < Reader
    end
  end
end
