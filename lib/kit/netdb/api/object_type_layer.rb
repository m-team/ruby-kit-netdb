require 'kit/netdb/api/layer'
require 'kit/netdb/api/function'


module KIT::NetDB::API
  class ObjectTypeLayer < Layer
    sublayer Function

    def initialize *args, name
      super(*args)

      @name = name
    end

    def name
      @name ||= up.index[id].name
    end

    def function_index idx = "FunctionIndex"
      sublayers = map_values(get_index[idx]) {|id, args| sublayer(
                                                id,
                                                args["doc"],
                                                args["params_dict"],
                                                args["mode"],
                                                type: Function)}
      deep_symbolize_keys sublayers
    rescue KIT::NetDB::IndexNotFoundError
      raise KIT::NetDB::ObjectTypeNotFoundError, "No object type named #{id}"
    end

    alias_method :index, :function_index

    def reader name
      # TODO: We assume readers to be inspectable, can we?
      function(Function::Inspectable, name)
    end

    def writer name
      function(Function::Writer, name)
    end

    def inspector name
      function(Function::Inspector, name)
    end

    def read name, *args, **kwargs
      reader(name).call(*args, **kwargs)
    end

    def write name, *args, **kwargs
      writer(name).call(*args, **kwargs)
    end

    def inspect name, *args, **kwargs
      inspector(name).call(*args, **kwargs)
    end

    alias_method :r, :read
    alias_method :w, :write
    alias_method :i, :inspect

    private

    def function type, name
      sublayer name.to_s, nil, nil, type: type
    end

    def self.define_sublayer_method id
      define_method id do |*args, **kwargs|
        sublayer(id, nil, nil).call(*args, **kwargs)
      end
    end

    def root
      up.up.up
    end

    def this_version
      up.up
    end

    def system
      up
    end
  end
end
