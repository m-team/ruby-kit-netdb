require 'kit/netdb/api/layer'
require 'kit/netdb/api/system_layer'


module KIT::NetDB::API
  class VersionLayer < Layer
    sublayer SystemLayer

    def initialize *args, features
      super(*args)

      @features = deep_symbolize_keys features if features
    end

    def features
      @features ||= up.index[id].features
    end

    def system_index idx = "SystemIndex"
      sublayers = map_values(get_index[idx]) {|id, name| sublayer id, name}
      deep_symbolize_keys sublayers
    rescue KIT::NetDB::IndexNotFoundError
      raise KIT::NetDB::VersionNotFoundError, "WebAPI Version #{id} not available"
    end

    alias_method :index, :system_index

    def system name
      sublayer name.to_s, name
    end

    alias_method :s, :system

    private

    def root
      up
    end
  end
end
