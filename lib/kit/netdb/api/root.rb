require 'kit/netdb/api/layer'
require 'kit/netdb/api/version_layer'
require 'kit/netdb/api/versions'


module KIT::NetDB::API
  class Root < Layer
    sublayer VersionLayer,
             :"2.1" => Versions::Two,
             :"2.0" => Versions::Two

    def initialize connection, *path
      @connection = connection
      @path = path.join '/'
    end

    def version_num_index idx = "VersionNumIndex"
      map_values(get_index[idx]) {|version, features| sublayer version, features}
    rescue KIT::NetDB::IndexNotFoundError
      raise KIT::NetDB::APINotFound, "No WebAPI found at #{@path}"
    end

    alias_method :index, :version_num_index

    def version num
      sublayer num, nil
    end

    alias_method :v, :version

    def default
      version "2.1"
    end
  end
end
