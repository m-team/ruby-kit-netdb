# coding: utf-8
require 'kit/netdb/api/system_layer'
require 'kit/netdb/api/object_types.rb'

module KIT::NetDB::API
  module Systems
    class WebAPI < SystemLayer
      sublayer datadict: ObjectTypes::WebAPI::DataDict,
               # TODO transaction: ObjectTypes::WebAPI::Transaction,
               version_status: ObjectTypes::WebAPI::VersionStatus
    end

    class DNSVS < SystemLayer
      sublayer fqdn: ObjectTypes::DNSVS::FQDN,
               record_inttype: ObjectTypes::DNSVS::RecordIntType,
               ns: ObjectTypes::DNSVS::NameServer,
               zone: ObjectTypes::DNSVS::ZoneDefinition,
               mgr2range: ObjectTypes::DNSVS::Manager2Range,
               fqdn_inttype: ObjectTypes::DNSVS::FQDNIntType,
               zone_deleg: ObjectTypes::DNSVS::ZoneDelegation,
               range: ObjectTypes::DNSVS::IPAddressRange,
               domain2range: ObjectTypes::DNSVS::Domain2Range,
               ipaddr: ObjectTypes::DNSVS::IPAddress,
               record: ObjectTypes::DNSVS::ResourceRecord
    end

    class DHCPVS < SystemLayer
      sublayer subnet_option: ObjectTypes::DHCPVS::SubnetOption,
               og: ObjectTypes::DHCPVS::OptionGroup,
               macfilter: ObjectTypes::DHCPVS::MACFilter,
               og2lease: ObjectTypes::DHCPVS::OptionGroup2Lease,
               ipaddr: ObjectTypes::DHCPVS::IPAddress,
               lease: ObjectTypes::DHCPVS::Lease,
               global_option: ObjectTypes::DHCPVS::GlobalOption,
               group_option: ObjectTypes::DHCPVS::GroupOption
    end

    class NetworkManagement < SystemLayer
      # TODO
    end

    class NetworkDokumentation < SystemLayer
      # TODO
    end
  end
end
