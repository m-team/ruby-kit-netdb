require 'kit/netdb/api/layer'
require 'kit/netdb/api/object_type_layer'


module KIT::NetDB::API
  class SystemLayer < Layer
    sublayer ObjectTypeLayer

    def initialize *args, name
      super(*args)

      @name = name
    end

    def name
      @name ||= up.index[id].name
    end

    def object_type_index idx = "ObjectTypeIndex"
      sublayers = map_values(get_index[idx]) {|id, name| sublayer id, name}
      deep_symbolize_keys sublayers
    rescue KIT::NetDB::IndexNotFoundError
      raise KIT::NetDB::SystemNotFoundError, "No system named #{id}"
    end

    alias_method :index, :object_type_index

    def object_type name
      sublayer name.to_s, nil
    end

    alias_method :ot, :object_type

    private

    def root
      up.up
    end

    def this_version
      up
    end

  end
end
