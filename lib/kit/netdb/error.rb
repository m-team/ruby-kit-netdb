module KIT::NetDB
  class Error < RuntimeError
  end

  class ServerError < Error
  end

  class ClientError < Error
  end

  class FunctionNotFoundError < ClientError
  end

  class IndexNotFoundError < ClientError
  end

  class NoAPIFound < IndexNotFoundError
  end

  class ObjectTypeNotFoundError < IndexNotFoundError
  end

  class SystemNotFoundError < IndexNotFoundError
  end

  class VersionNotFoundError < IndexNotFoundError
  end

  class FunctionalError < ClientError
    def initialize response
      @response = response
    end

    def [](key)
      @response[key]
    end

    def error_code
      self[:error][:data][:error_code]
    end

    def db_error_code
      self[:error][:type][:code].split("-")[1]
    end

    def to_s
      "%s: %s: %s (%s: %s)" % [
        super,
        self[:error][:data][:error_code],
        self[:error][:data][:code_descr],
        self[:error][:type][:code],
        self[:error][:type][:text_descr]]
    rescue NoMethodError
      "%s: %s" % [super, @response]
    end
  end
end
